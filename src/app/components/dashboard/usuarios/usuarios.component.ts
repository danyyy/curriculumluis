import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { UsuarioDataI } from 'src/app/interface/usuario.interface';
import { UsuarioService } from 'src/app/services/usuario.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit ,AfterViewInit{
  listUsuarios:UsuarioDataI[]=[];
  displayedColumns: string[] = ['nombres','apellidos','email','celular','fecha','hora','descripcion','acciones'];
  dataSource !: MatTableDataSource<any>;
  estadoLista!:boolean;
  radio= '';

  @ViewChild(MatPaginator)paginator!:MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(private _usuarioService:UsuarioService, 
              private _snackbar:MatSnackBar, 
              private router:Router) { 

              }

  ngOnInit(): void {
    
    console.log(new Date().getMonth());
    
    this.cargarUsuarios();
  }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort      = this.sort;
  }

  cargarUsuarios(){
    this.listUsuarios = this._usuarioService.getUsuario();
   // console.log(this.listUsuarios[0].usuario.getTime());
    this.dataSource   = new MatTableDataSource(this.listUsuarios);
    this.dataSource.paginator=this.paginator;
    this.dataSource.sort=this.sort;

  }

  applyFilter(event:Event){
    const filterValue=(event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();


  }


  eliminarUsuario(usuario:any){
    const opcion = confirm('Estas seguro de eliminar el mensaje');
    if (opcion) {
      
     console.log(usuario);
    this._usuarioService.eliminarUsuario(usuario)
    this.cargarUsuarios();

    this._snackbar.open('El mensaje fue eliminado con exito', '', {
      duration: 1500,
      horizontalPosition: 'center',
      verticalPosition: 'bottom',
    });
  }
  }

  verUsuario(usuario: any){
    console.log(usuario);
    this.router.navigate(['dashboard/ver-usuario', usuario]);
  }


  modificarUsuario(usuario: any){
    console.log(usuario);
    this.router.navigate(['dashboard/crear-usuario', usuario]);
  }


  agregarLike() {
    /*
    EN ESTA CONDICION, ESTAMOS DICIENDO: 
    "SI NO EXISTE LA VARIABLE DE localStorage superheroe"
    REALIZAREMOS LO SIGUIENTE
    */
  }

}
