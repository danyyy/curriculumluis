import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioService } from '../../../../services/usuario.service';

@Component({
  selector: 'app-ver-usuario',
  templateUrl: './ver-usuario.component.html',
  styleUrls: ['./ver-usuario.component.css']
})
export class VerUsuarioComponent implements OnInit {

  form!:FormGroup;

  constructor(private activeRoute: ActivatedRoute, private usuarioService: UsuarioService,private router:Router, private fb:FormBuilder) { 
    this.activeRoute.params.subscribe(params => {
      const id = params['id'];

      const usuario = this.usuarioService.buscarUsuario(id);

      //Verifico que la longitud del objeto es cero
      if (Object.keys(usuario).length===0) {
        this.router.navigate(['/dashboard/usuarios']);
      }

      this.form = this.fb.group({
        nombre: ['', Validators.required],
        apellido: ['', Validators.required],
        email: ['', Validators.required],
        celular: ['', Validators.required],
        descripcion: ['', Validators.required],
        fecha: ['', Validators.required],
        hora: ['', Validators.required],
      });

      this.form.patchValue({
        nombre: usuario.nombres,
        apellido: usuario.apellidos,
        email: usuario.email,
        celular: usuario.celular,
        descripcion: usuario.descripcion,
        fecha: usuario.fecha,
        hora: usuario.hora,

      });


    })
  }

  ngOnInit(): void {
  }

  Volver(){
    this.router.navigate(['/dashboard/usuarios']);

  }

}
