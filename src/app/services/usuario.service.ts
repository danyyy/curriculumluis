
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { UsuarioDataI } from 'src/app/interface/usuario.interface';


@Injectable({
  providedIn: 'root'
})



export class UsuarioService {

  listUsuarios: UsuarioDataI[] = [
    {id : '1', nombres: 'Camila', apellidos: 'Perez',celular:7146545, email: 'camiz@gmail.com',fecha:'2022-08-18T14:26',hora: '2022-06-28T19:30',descripcion:'Quisiera agendar una cita'},
    {id : '2', nombres: 'Roxana', apellidos: 'Gomez',celular:714655, email: 'roxz@gmail.com',fecha:'2022-06-28T14:30',hora:'2022-06-28T13:30',descripcion:'Reunion'},
    {id : '3', nombres: 'Celia', apellidos: 'Garcia',celular:7144665, email: 'celz@gmail.com',fecha:'2022-09-28T10:00',hora:'2022-06-28T10:00',descripcion:'Quisiera agendar una cita'},
    {id : '4', nombres: 'Roman', apellidos: 'Liop' ,celular:7145455, email: 'romz@gmail.com',fecha:'2022-08-10T14:06',hora:'2022-06-28T14:09',descripcion:'Reunion'},
    {id : '5', nombres: 'Martin', apellidos: 'Marino' ,celular:7145665, email: 'maroz@gmail.com',fecha:'2022-04-20T13:00',hora:'2022-06-28T09:20',descripcion:'Quisiera agendar una cita'},
    {id : '6', nombres: 'Nancy', apellidos: 'Mendizabal',celular:6020626, email: 'nanz@gmail.com',fecha:'2022-03-15T15:26',hora:'2022-06-28T08:15',descripcion:'Reunion'}
  ];

  

  constructor(private router:Router) {
    this.cargarDatos();
   }

  getUsuario(): UsuarioDataI[] {
    //slice retorna  una copia del array
    return this.listUsuarios.slice();
  }


  eliminarUsuario(usuario:any){
    this.cargarDatos();
    this.listUsuarios =this.listUsuarios.filter(data => {
      return data.id.toString() !== usuario.toString(); 
    })
    localStorage.setItem("listaAgenda", JSON.stringify( this.listUsuarios));
    this.cargarDatos();
  }

  agregarUsuario(usuario:UsuarioDataI){

    this.listUsuarios.unshift(usuario);
    localStorage.setItem("listaAgenda", JSON.stringify( this.listUsuarios));
    this.cargarDatos();

  }

   buscarUsuario(id: any): UsuarioDataI{
     //o retorna un json {} vacio
     return this.listUsuarios.find(element => element.id == id) || {} as UsuarioDataI;
   }


  modificarUsuario(user: UsuarioDataI){
    this.cargarDatos();
    this.eliminarUsuario(user.id);
    this.agregarUsuario(user);
  }

  cargarDatos(){
    if (!localStorage.getItem("listaAgenda")) {

      console.log(this.listUsuarios);
      localStorage.setItem("listaAgenda", JSON.stringify( this.listUsuarios));
      let guardados = localStorage.getItem('listaAgenda');
      this.listUsuarios = JSON.parse(guardados || '{}');
      console.log(this.listUsuarios[0]);

    } else{
      
      //localStorage.setItem("listaAgenda", JSON.stringify( this.listUsuarios));
      let guardados = localStorage.getItem('listaAgenda');
      this.listUsuarios = JSON.parse(guardados || '{}');
    }
  }

}
